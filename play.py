import chess
import human
import randomAI
import decisionTreeAI

def play(printGame = True):
    board = chess.Board()
    # board = chess.Board('5K2/8/4B3/8/3B4/8/7p/7k')
    # board = chess.Board('rnbq1b1r/2p1pkpp/7n/pp1p1p2/1P4P1/5K2/P1PPPPBP/RNBQ2NR')
    # board = chess.Board('8/7P/8/7K/8/8/7k/8')
    if printGame:
        print(board)
        print("Let's start with this game of chess")

    # player1 = human.Human("Player 1")
    # player2 = randomAI.RandomAI("Player 2")
    # player1 = decisionTreeAI.DecisionTreeAI("Player 1", chess.WHITE, 0)
    player2 = decisionTreeAI.DecisionTreeAI("Player 2", chess.BLACK, 2)
    player1 = decisionTreeAI.DecisionTreeAI("Player 1", chess.WHITE, 2, False, 0, True)
    # player2 = decisionTreeAI.DecisionTreeAI("Player 2", chess.BLACK, 3, False, 0.2)
    # player1 = decisionTreeAI.DecisionTreeAI("Player 1", chess.WHITE, 3, False, 0, True, False)
    # player2 = decisionTreeAI.DecisionTreeAI("Player 2", chess.BLACK, 3, False, 0, False, True)
    # player2 = decisionTreeAI.DecisionTreeAI("Player 2", chess.BLACK, 3, True)
    # player2 = scikitLinearAI.ScikitLinearAI("Player 2", chess.BLACK, 1, True)

    # The main game loop, as provided by the creator.
    while (not board.is_game_over()):
        if printGame:
            print('\n', player1.description, 'will now play')

        player1Move = player1.movesun(board)
        board.push(player1Move)

        if printGame:
            print(board)

        if printGame:
            print('\n', player2.description, 'will now play')

        player2Move = player2.move(board)
        if player2Move == -1:
            if printGame:
                print('\nResult:', board.result())
                print('Number of plays:', board.fullmove_number)
            return board
        board.push(player2Move)

        if printGame:
            print(board)

    if printGame:
        print('\nResult:', board.result())
        print('Number of plays:', board.fullmove_number)

    return board


if __name__ == '__main__':
    play()